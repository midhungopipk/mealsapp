import { MEALS } from "../../data/dummy-data";
import { SET_FILTERS, TOGGLE_FAVORITE } from "../actions/meals";

const initialState = {
	meals: MEALS,
	filteredMeals: MEALS,
	favoriteMeals: [],
};

const mealsReducer = (state = initialState, action) => {
	switch (
		action.type //Logic of adding a meal as favourite
	) {
		case TOGGLE_FAVORITE:
			const existingIndex = state.favoriteMeals.findIndex(
				(meal) => meal.id === action.mealId
			);
			if (existingIndex >= 0) {
				const updatedFavMeals = [...state.favoriteMeals];
				updatedFavMeals.splice(existingIndex, 1);
				return { ...state, favoriteMeals: updatedFavMeals };
			} else {
				//video 155  8:27 duration
				const meal = state.meals.find(
					(meal) => meal.id === action.mealId
				);
				return {
					...state,
					favoriteMeals: state.favoriteMeals.concat(meal),
				};
			}
		case SET_FILTERS:
			const appliedFilters = action.filters;
			const UpdatedFilteredMeals = state.meals.filter((meal) => {
				if (appliedFilters.glutenFree && !meal.isGlutenFree) {
					return false;
				}
				if (appliedFilters.lactoseFree && !meal.lactoseFree) {
					return false;
				}
				if (appliedFilters.Vegitarian && !meal.isVegitarian) {
					return false;
				}
				if (appliedFilters.Vegan && !meal.isVegan) {
					return false;
				}
				return true;
			});
			return { ...state, filteredMeals: UpdatedFilteredMeals };
		default:
			return state;
	}
};

export default mealsReducer;
