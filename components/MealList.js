import React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { useSelector } from "react-redux";
import MealItem from "../components/MealItem";

const MealList = (props) => {
	// const favouriteMeals = useSelector((state) => state.meals.favouriteMeals);

	const renderMealItem = (itemData) => {
		// const isFavorite = favouriteMeals.find(
		// 	(meal) => meal.id === itemData.item.id
		// );

		return (
			<MealItem
				title={itemData.item.title}
				duration={itemData.item.duration}
				complexity={itemData.item.complexity}
				affordability={itemData.item.affordability}
				image={itemData.item.imageUrl}
				onSelectMeal={() => {
					props.navigation.navigate({
						routeName: "MealDetails",
						params: {
							mealId: itemData.item.id,
							mealTitle: itemData.item.title,
							// isFav: isFavorite,
						},
					});
				}}
			/>
		);
	};
	return (
		<View style={styles.screen}>
			<FlatList
				data={props.listData}
				keyExtractor={(item, index) => item.id}
				renderItem={renderMealItem}
				style={{ width: "100%" }}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	screen: {
		flex: 1,
		alignItems: "center",
		justifyContent: "center",
	},
});

export default MealList;
