import React from "react";
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	ImageBackground,
} from "react-native";

const MealItem = (props) => {
	return (
		<View style={{ padding: 10 }}>
			<View style={styles.mealItem}>
				<TouchableOpacity onPress={props.onSelectMeal}>
					<View>
						<View
							style={{ ...styles.mealRow, ...styles.mealHeader }}
						>
							<ImageBackground
								source={{ uri: props.image }}
								style={styles.bgImage}
							>
								<View style={styles.titleContainer}>
									<Text
										style={styles.title}
										numberOfLines={1}
									>
										{props.title}
									</Text>
								</View>
							</ImageBackground>
						</View>
						<View
							style={{ ...styles.mealRow, ...styles.mealDetails }}
						>
							<Text style={styles.mealSpecs}>
								{props.duration}m
							</Text>
							<Text style={styles.mealSpecs}>
								{props.complexity.toUpperCase()}
							</Text>
							<Text style={styles.mealSpecs}>
								{props.affordability.toUpperCase()}
							</Text>
						</View>
					</View>
				</TouchableOpacity>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	mealRow: {
		flexDirection: "row",
	},
	mealItem: {
		backgroundColor: "#f5f5f5", //light gray color
		width: "100%",
		height: 250,
		borderRadius: 15,
		overflow: "hidden",
		elevation: 10,
	},
	mealHeader: {
		height: "90%",
	},
	mealDetails: {
		paddingHorizontal: 10,
		justifyContent: "space-between",
		alignItems: "center",
		height: "10%",
	},
	bgImage: {
		height: "100%",
		width: "100%",
		justifyContent: "flex-end",
	},
	title: {
		fontWeight: "bold",
		color: "white",
		textAlign: "center",
		fontSize: 20,
	},
	titleContainer: {
		backgroundColor: "rgba(0,0,0,0.5)",
		paddingVertical: 5,
		paddingHorizontal: 12,
	},
	mealSpecs: {
		fontWeight: "bold",
	},
});

export default MealItem;
