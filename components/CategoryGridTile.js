import React from "react";
import {
	View,
	Text,
	StyleSheet,
	TouchableOpacity,
	Platform,
	TouchableNativeFeedback,
} from "react-native";

const CategoryGridTile = (props) => {
	let TouchableComponent = TouchableOpacity;
	if (Platform.OS === "android" && Platform.Version >= 21) {
		TouchableComponent = TouchableNativeFeedback;
	}
	return (
		<View style={styles.gridItem}>
			<TouchableComponent onPress={props.onPress}>
				<View
					style={{
						...styles.gridContainer,
						...{ backgroundColor: props.color },
					}}
				>
					<Text style={styles.title}>{props.title}</Text>
				</View>
			</TouchableComponent>
		</View>
	);
};

const styles = StyleSheet.create({
	gridItem: {
		flex: 1,
		margin: 20,
		borderColor: "black",
		borderWidth: 3,
		height: 200,
		borderRadius: 15,
		overflow: "hidden",
		elevation: 10,
		shadowColor: "black",
		shadowOpacity: 0.26,
		shadowOffset: { width: 0, height: 2 },
		shadowRadius: 10,
	},
	gridContainer: {
		flex: 1,
		justifyContent: "flex-end",
		alignItems: "flex-end",
		padding: 10,
	},
	title: {
		fontWeight: "bold",
		fontSize: 22,
	},
});

export default CategoryGridTile;
