import React from "react";
import { HeaderButton } from "react-navigation-header-buttons";
import { Ionicons } from "@expo/vector-icons";
import { Platform } from "react-native";
import Colors from "../constants/Colors";

const CustomHeaderButton = (props) => {
	return (
		<HeaderButton
			{...props} //to recieve every props that we are getting(IMPORTANT)
			IconComponent={Ionicons}
			iconSize={29}
			color={Platform.OS === "android" ? "white" : Colors.primary}
		/>
	);
};

export default CustomHeaderButton;
