import React from "react";
import {
	View,
	Text,
	StyleSheet,
	Button,
	Platform,
	FlatList,
} from "react-native";
import { CATEGORIES } from "../data/dummy-data";
import { useSelector } from "react-redux";
import Colors from "../constants/Colors";
import MealItem from "../components/MealItem";
import MealList from "../components/MealList";

const CategoryMealsScreen = (props) => {
	const availableMeals = useSelector((state) => state.meals.filteredMeals);

	const catId = props.navigation.getParam("categoryId");
	// const selectedCategory = CATEGORIES.find((cat) => cat.id === catId);
	const displayedMeals = availableMeals.filter(
		(meal) => meal.categoryIds.indexOf(catId) >= 0
	);
	if (displayedMeals.length === 0) {
		return (
			<View style={styles.content}>
				<Text style={{ fontWeight: "bold", fontSize: 22 }}>
					No Meals to display!!! Check your Filters
				</Text>
			</View>
		);
	}
	//catId we extracted from the route params is >=0 because that will be minus one
	//if the category ID is not part of the category IDs, if it's zero or greater, then we know that
	//this meal has this category ID in its category IDs array and this will give us an
	//array of displayed meals for that selected category,

	// const renderMealItem = (itemData) => {
	// 	return (
	// 		<MealItem
	// 			title={itemData.item.title}
	// 			duration={itemData.item.duration}
	// 			complexity={itemData.item.complexity}
	// 			affordability={itemData.item.affordability}
	// 			image={itemData.item.imageUrl}
	// 			onSelectMeal={() => {
	// 				props.navigation.navigate({
	// 					routeName: "MealDetails",
	// 					params: {
	// 						mealId: itemData.item.id,
	// 					},
	// 				});
	// 			}}
	// 		/>
	// 	);
	// };

	return <MealList listData={displayedMeals} navigation={props.navigation} />;
};

CategoryMealsScreen.navigationOptions = (navigationData) => {
	const catId = navigationData.navigation.getParam("categoryId");
	const selectedCategory = CATEGORIES.find((cat) => cat.id === catId);

	return {
		headerTitle: selectedCategory.title,
		headerStyle: {
			backgroundColor:
				Platform.OS === "android" ? selectedCategory.color : "white",
		},
		headerTintColor:
			Platform.OS === "android" ? "white" : selectedCategory.color,
	};
};

const styles = StyleSheet.create({
	content: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

export default CategoryMealsScreen;
