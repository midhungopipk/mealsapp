import React from "react";
import { View, Text, StyleSheet } from "react-native";
import MealList from "../components/MealList";
import { useSelector } from "react-redux";
import FiltersScreen from "./FiltersScreen";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";

const FavoritesScreen = (props) => {
	const FavMeals = useSelector((state) => state.meals.favoriteMeals);

	if (FavMeals.length === 0 || !FavMeals) {
		return (
			<View style={styles.content}>
				<Text style={{ fontWeight: "bold", fontSize: 22 }}>
					No favourites found!!!.Try to add some.
				</Text>
			</View>
		);
	}

	return <MealList listData={FavMeals} navigation={props.navigation} />;
};
FavoritesScreen.navigationOptions = (navData) => {
	return {
		headerStyle: {
			backgroundColor: "indigo",
		},
		headerTitle: "Your Favorites",
		headerLeft: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="Menu"
					iconName="ios-menu"
					onPress={() => {
						navData.navigation.toggleDrawer();
					}}
				/>
			</HeaderButtons>
		),
	};
};

const styles = StyleSheet.create({
	content: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
});

export default FavoritesScreen;
