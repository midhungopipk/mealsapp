import React, { useEffect, useCallback } from "react";
import { ScrollView, View, Image, Text, StyleSheet } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import { useSelector, useDispatch } from "react-redux";
import HeaderButton from "../components/HeaderButton";
import { toggleFavourite } from "../store/actions/meals";

const ListItem = (props) => {
	return (
		<View style={styles.listItem}>
			<Text>{props.children}</Text>
		</View>
	);
};

const MealDetailScreen = (props) => {
	const availableMeals = useSelector((state) => state.meals.meals);
	const mealId = props.navigation.getParam("mealId");

	const currentMealIsFavourite = useSelector((state) =>
		state.meals.favoriteMeals.some((meal) => meal.id === mealId)
	);

	const selectedMeal = availableMeals.find((meal) => meal.id === mealId);

	const dispatch = useDispatch();

	const toggleFavouriteHandler = useCallback(() => {
		dispatch(toggleFavourite(mealId));
	}, [dispatch, mealId]);

	useEffect(() => {
		// props.navigation.setParams({ mealsTitle: selectedMeal.title });
		props.navigation.setParams({ toggleFav: toggleFavouriteHandler });
	}, [toggleFavouriteHandler]);

	useEffect(() => {
		props.navigation.setParams({ isFav: currentMealIsFavourite });
	}, [currentMealIsFavourite]);

	return (
		<ScrollView>
			<Image
				source={{ uri: selectedMeal.imageUrl }}
				style={styles.image}
			/>
			<View style={styles.details}>
				<Text>{selectedMeal.duration}m</Text>
				<Text>{selectedMeal.complexity.toUpperCase()}</Text>
				<Text>{selectedMeal.affordability.toUpperCase()}</Text>
			</View>
			<View style={styles.title}>
				<Text style={styles.title}>Ingredients...!!</Text>
				{selectedMeal.ingredients.map((ingredient) => (
					<ListItem key={ingredient}>{ingredient}</ListItem>
				))}
				<Text style={styles.title}>Steps</Text>

				{selectedMeal.steps.map((step) => (
					<ListItem key={step}>{step}</ListItem>
				))}
			</View>
		</ScrollView>
	);
};

MealDetailScreen.navigationOptions = (navigationData) => {
	// const mealId = navigationData.navigation.getParam("mealId");
	const mealsTitle = navigationData.navigation.getParam("mealsTitle");
	// const selectedMeal = MEALS.find((meal) => meal.id === mealId);
	const toggleFavourite = navigationData.navigation.getParam("toggleFav");
	const isFavorite = navigationData.navigation.getParam("isFav");
	return {
		headerTitle: mealsTitle,
		headerRight: () => (
			<HeaderButtons HeaderButtonComponent={HeaderButton}>
				<Item
					title="favorite"
					iconName={isFavorite ? "ios-star" : "ios-star-outline"}
					onPress={toggleFavourite}
				/>
			</HeaderButtons>
		),
	};
};

const styles = StyleSheet.create({
	image: {
		width: "100%",
		height: 200,
	},
	details: {
		flexDirection: "row",
		padding: 15,
		justifyContent: "space-around",
	},
	title: {
		fontWeight: "bold",
		fontSize: 22,
		textAlign: "center",
	},
	listItem: {
		marginVertical: 10,
		marginHorizontal: 20,
		borderColor: "grey",
		borderWidth: 2,
		padding: 10,
	},
});

export default MealDetailScreen;
